﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1DiffTestAndChart
{
    class Program
    {
        private static Tester tester = new Tester();
        static void Main(string[] args)
        {
            int minSize = 100, maxSize = 1000, step = 100;
            string generalPartOfFileName = "out\\";
            MatrixGenerator<int> gen = new MatrixGenerator<int>(new IntGenerator(-10, 10));
            for (var size = minSize; size <= maxSize; size += step)
            {
                Matrix<int> intMatr = gen.generateMatrix(size, size);
                intMatr.writeToFile(generalPartOfFileName + "matr"+size+".txt");
            }
            
//            IList<Point> listTrad = tester.testTraditional(minSize, maxSize, step);
//            ListToFileWriter.write(listTrad, generalPartOfFileName + "listTrad1000.txt");
            /*
            IList<Point> listGrape = tester.testGrapeAlg(minSize, maxSize, step);
            IList<Point> listOpGrape = tester.testGrapeAlg(minSize, maxSize, step);
            */
        }
    }
}
