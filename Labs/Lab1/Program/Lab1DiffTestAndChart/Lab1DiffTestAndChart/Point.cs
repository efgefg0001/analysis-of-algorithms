﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1DiffTestAndChart
{
    public struct Point
    {
        public readonly double x;
        public readonly double y;
        public Point(double x = 0, double y = 0)
        {
            this.x = x;
            this.y = y;
        }
        public override string ToString()
        {
            return x + " " + y; 
        }
    }
}
