﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1DiffTestAndChart
{
    class Vector<T>
    {
        private T[] arr;
        public Vector(int length)
        {
            Length = length;
            arr = new T[Length];
        }
        public T this[int i]
        {
            get
            {
                return arr[i];
            }
            set
            {
                arr[i] = value;
            }
        }
        public int Length { get; private set; }
    }
}
