﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab1DiffTestAndChart
{
    class ListToFileWriter
    {
        public static void write(IList<Point> list, string filename)
        {
            try
            {
                using(StreamWriter file = new StreamWriter(filename))
                {
                    foreach (var point in list)
                        file.WriteLine(point.ToString());
                }
            }
            catch(Exception error)
            {
                Console.WriteLine(error);
            }
        }
    }
}
