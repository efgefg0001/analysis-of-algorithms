﻿using System.IO;
using System;

namespace Lab1DiffTestAndChart
{
    public class Matrix<T>
    {
        private T[,] arr;
        public Matrix(int rowsNum, int columnsNum)
        {
            RowsNum = rowsNum;
            ColumnsNum = columnsNum;
            arr = new T[RowsNum, ColumnsNum];
        }
        public T this[int i, int j]
        {
            get
            {
                return arr[i, j];
            }
            set
            {
                arr[i, j] = value;
            }
        }
        public int RowsNum { get; private set; }
        public int ColumnsNum{ get; private set; }
        public void writeToFile( string fileName)
        {
            try
            {
                using(StreamWriter file = new StreamWriter(fileName))
                {
                    for (var i = 0; i < RowsNum; ++i)
                    {
                        for (var j = 0; j < ColumnsNum; ++j)
                            file.Write(this[i, j] + " ");
                        file.WriteLine();
                    }
                }
            }
            catch(Exception error)
            {
                Console.WriteLine(error);
            }
        }
 
    }
}
