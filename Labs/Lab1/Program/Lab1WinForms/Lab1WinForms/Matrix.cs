﻿namespace Lab1
{
    public class Matrix<T>
    {
        private T[,] arr;
        public Matrix(int rowsNum, int columnsNum)
        {
            RowsNum = rowsNum;
            ColumnsNum = columnsNum;
            arr = new T[RowsNum, ColumnsNum];
        }
        public T this[int i, int j]
        {
            get
            {
                return arr[i, j];
            }
            set
            {
                arr[i, j] = value;
            }
        }
        public int RowsNum { get; private set; }
        public int ColumnsNum{ get; private set; }

    }
}
