﻿namespace Lab1
{
    public class MatrixMultiplication
    {
        private MatrixMultiplication() { }
        internal static bool CanMultiply<Type>(Matrix<Type> factorA, Matrix<Type> factorB)
        {
            return factorA.ColumnsNum == factorB.RowsNum ? true : false;
        }
 
       internal static void OpGrapeAlg(ref Matrix<int> result, Matrix<double> factorA, Matrix<double> factorB, 
            Vector<double> mulH, Vector<double> mulV)
        {
           
        }




        internal static void Traditional(Matrix<int> result, Matrix<int> factorA, Matrix<int> factorB)
        {
            for (var i = 0; i < factorA.RowsNum; ++i)
            {
                for (var j = 0; j < factorB.ColumnsNum; ++j)
                {
                    result[i, j] = 0;
                    for (var k = 0; k < factorA.ColumnsNum; ++k)
                        result[i, j] += factorA[i, k] * factorB[k, j];
                }
            } 
        }



        internal static void GrapeAlg(Matrix<int> result, Matrix<int> factorA, Matrix<int> factorB, Vector<int> mulH, Vector<int> mulV)
        {
             for (var i = 0; i < factorA.RowsNum; ++i)
                for (var j = 0; j < factorA.ColumnsNum / 2; ++j)
                    mulH[i] += factorA[i, j * 2] * factorA[i, j * 2 + 1];
            for (var i = 0; i < factorB.ColumnsNum; ++i)
                for (var j = 0; j < factorB.RowsNum / 2; ++j)
                    mulV[i] += factorB[j * 2, i] * factorB[j * 2 + 1, i];
            for (var i = 0; i < factorA.RowsNum; ++i)
                for (var j = 0; j < factorB.ColumnsNum; ++j)
                {
                    result[i,j] = -mulH[i] - mulV[j];
                    for (var k = 0; k < factorA.ColumnsNum / 2; ++k)
                        result[i, j] += (factorA[i, k*2] + factorB[k*2+1, j]) * (factorA[i, k*2 +1] + factorB[k*2,j]);
                }
            if(factorA.ColumnsNum % 2 == 1)
            {
                for (var i = 0; i < factorA.RowsNum; ++i )
                    for (var j = 0; j < factorB.ColumnsNum; ++j)
                        result[i,j] += factorA[i, factorA.ColumnsNum - 1] * factorB[factorA.ColumnsNum - 1, j];
            }
        }

        internal static void OpGrapeAlg(Matrix<int> result, Matrix<int> factorA, Matrix<int> factorB, 
            Vector<int> mulH, Vector<int> mulV)
        {
            bool flag = factorA.ColumnsNum % 2 == 1;
            int idx = factorA.ColumnsNum - 1;
            for (var i = 0; i < factorA.RowsNum; ++i)
                for (var j = 0; j < idx; j+=2)
                    mulH[i] -= factorA[i, j] * factorA[i, j + 1];
            for (var i = 0; i < factorB.ColumnsNum; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulV[i] -= factorB[j, i] * factorB[j + 1, i];
            for (var i = 0; i < factorA.RowsNum; ++i)
                for (var j = 0; j < factorB.ColumnsNum; ++j)
                {
                    result[i,j] += mulH[i] + mulV[j];
                    for (var k = 0; k < idx; k+=2)
                        result[i, j] += (factorA[i, k] + factorB[k+1, j]) * (factorA[i, k+1] + factorB[k,j]);
                    if (flag)
                        result[i, j] += factorA[i, idx] * factorB[idx, j];
                }
 
        }

    }
}
