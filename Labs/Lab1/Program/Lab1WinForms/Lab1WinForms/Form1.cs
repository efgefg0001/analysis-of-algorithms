﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab1;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab1WinForms
{
    public partial class MainForm : Form
    {
        private Tester tester = new Tester();
        private void fillSeries(Series series, IList<Point> points)
        {
            foreach (var point in points)
                series.Points.AddXY(point.X, point.Y);
        }

        private void runTests()
        {
            int minSize = 100, maxSize = 1000, step = 100;
//            Series serTrad = chart1.Series[0];
            IList<Point> points = tester.testTraditional(minSize, maxSize, step);
            fillSeries();
            foreach (var point in points)
                chart1.Series[0].Points.AddXY(point.X, point.Y);
         //   fillSeries(chart1.Series[0], points);
            points = tester.testGrapeAlg(minSize, maxSize, step);
            foreach (var point in points)
                chart1.Series[1].Points.AddXY(point.X, point.Y);
            points = tester.testOpGrapeAlg(minSize, maxSize, step);
            foreach (var point in points)
                chart1.Series[2].Points.AddXY(point.X, point.Y);
        }

        public MainForm()
        {
            InitializeComponent();
            runTests();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {

        }
    }
}
