﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1;

namespace Lab1
{
    public class MatrixGenerator<T> 
    {
        public MatrixGenerator(IElementGenerator<T> generator)
        {
            Generator = generator;
        }
        public Matrix<T> generateMatrix(int rowNum, int columnsNum)
        {
            Matrix<T> matr = new Matrix<T>(rowNum, columnsNum);
            for (var i = 0; i < matr.RowsNum; ++i)
                for (var j = 0; j < matr.ColumnsNum; ++j)
                    matr[i, j] = Generator.generateElement();
            return matr;
        }
        public int generateRandDivider(int number)
        {
            IList<int> listOfDividers = new List<int>();
            for (var i = 1; i <= number; ++i)
                if (number % i == 0)
                    listOfDividers.Add(i);
            Random rand = new Random();
            int index = rand.Next(0, listOfDividers.Count);
            return listOfDividers[index];
        }
        public IElementGenerator<T> Generator
        {
            get;
            private set;
        }

    }
}
