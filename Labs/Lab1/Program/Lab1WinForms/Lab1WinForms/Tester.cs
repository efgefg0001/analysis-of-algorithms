﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1;
using System.Drawing;

namespace Lab1
{
    public class Tester
    {
        private Matrix<int> factorA;
        private Matrix<int> factorB;
        private MatrixGenerator<int> matrGenerator; 
        public Tester()
        {
            matrGenerator = new MatrixGenerator<int>(new IntGenerator(0, 10));
//            factorA = matrGenerator.generateMatrix(3, 5);
//            factorB = matrGenerator.generateMatrix(5, 2);
        }
        public IList<Point> testTraditional(int minSize, int maxSize, int step)
        {
            IList<Point> listOfChartPoints = new List<Point>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                //int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(size, size);
                factorB = matrGenerator.generateMatrix(size, size);
                Matrix<int> result = new Matrix<int>(factorA.RowsNum, factorB.ColumnsNum);
                DateTime start = DateTime.Now;
                MatrixMultiplication.Traditional(result, factorA, factorB);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new Point(size, duration.Milliseconds));
            }
            return listOfChartPoints;

        }
        public IList<Point> testGrapeAlg(int minSize, int maxSize, int step)
        {
            IList<Point> listOfChartPoints = new List<Point>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                //int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(size, size);
                factorB = matrGenerator.generateMatrix(size, size);
                Matrix<int> result = new Matrix<int>(factorA.RowsNum, factorB.ColumnsNum);
                Vector<int> mulH = new Vector<int>(factorA.RowsNum);
                Vector<int> mulV = new Vector<int>(factorB.ColumnsNum);
                DateTime start = DateTime.Now;
                MatrixMultiplication.GrapeAlg(result, factorA, factorB, mulH, mulV);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new Point(size, duration.Milliseconds));
            }
            return listOfChartPoints;


               
        }
        public IList<Point> testOpGrapeAlg(int minSize, int maxSize, int step)
        {
            IList<Point> listOfChartPoints = new List<Point>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
        //        int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(size, size);
                factorB = matrGenerator.generateMatrix(size,size);
                Matrix<int> result = new Matrix<int>(factorA.RowsNum, factorB.ColumnsNum);
                Vector<int> mulH = new Vector<int>(factorA.RowsNum);
                Vector<int> mulV = new Vector<int>(factorB.ColumnsNum);
                DateTime start = DateTime.Now;
                MatrixMultiplication.OpGrapeAlg(result, factorA, factorB, mulH, mulV);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new Point(size, duration.Milliseconds));
            }
            return listOfChartPoints;
        }
    }
}
