﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;
using System.Collections.Generic;

namespace Matrix
{
    public partial class MainForm : Form
    {

        private Tester tester = new Tester(0, 10);
        public MainForm()
        {
            InitializeComponent();
            
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
//            TimeAnalysys(MatrixMultiplier.OrdinaryAlgorythm, MatrixMultiplier.VinogradAlgorythm, MatrixMultiplier.VinogradAlgorythmModificated,
//                chart1.Series["Ordinary"].Points, chart1.Series["Vinograd"].Points, chart1.Series["Vinograd_mod"].Points);
        }

        private void DrawChart(int index, IList<Point> points)
        {
            foreach (var point in points)
                chart1.Series[index].Points.AddXY(point.X, point.Y);
        }
        private void CleanSeries()
        {
            foreach (var series in chart1.Series)
                series.Points.Clear();
        }
        private void analyseButton_Click(object sender, EventArgs e)
        {
            try
            {
                CleanSeries();
                int minSize = int.Parse(minTextBox.Text);
                int maxSize = int.Parse(maxTextBox.Text);
                int step = int.Parse(stepTextBox.Text);
                DrawChart(0, tester.testTraditional(minSize, maxSize, step));
                DrawChart(1, tester.testGrapeAlg(minSize, maxSize, step));
                DrawChart(2, tester.testOpGrapeAlg(minSize, maxSize, step));
            }
            catch(Exception error)
            {
                MessageBox.Show(error.ToString());
            }
        }
    }
}
