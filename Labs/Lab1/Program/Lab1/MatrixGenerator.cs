﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix 
{
    public class MatrixGenerator<T> 
    {
        public MatrixGenerator(IElementGenerator<T> generator)
        {
            Generator = generator;
        }
        public T[,] generateMatrix(int rowNum, int columnsNum)
        {
            T[,] matr = new T[rowNum, columnsNum];
            for (var i = 0; i < rowNum; ++i)
                for (var j = 0; j < columnsNum; ++j)
                    matr[i, j] = Generator.generateElement();
            return matr;
        }
        public IElementGenerator<T> Generator
        {
            get;
            private set;
        }

    }
}
