﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    public class Point 
    {
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
        public double X { get; private set; }
        public double Y { get; private set; }
    }
    public class Tester
    {
        private int[,] factorA;
        private int[,] factorB;
        private MatrixGenerator<int> matrGenerator; 
        private double ConvertToMilliseconds(TimeSpan duration)
        {
            return duration.Milliseconds + 1000.0 * (duration.Seconds + duration.Minutes * 60.0); 
        }
        public Tester(int minValue, int maxValue)
        {
            matrGenerator = new MatrixGenerator<int>(new IntGenerator(minValue, maxValue));
        }
        public IList<Point> testTraditional(int minSize, int maxSize, int step)
        {
            IList<Point> listOfChartPoints = new List<Point>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                //int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(size, size);
                factorB = matrGenerator.generateMatrix(size, size);
                int[,] result = new int[factorA.GetLength(0),factorB.GetLength(1)]; 
                DateTime start = DateTime.Now;
                MatrixMultiplication.Traditional(ref result, factorA, factorB);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new Point(size, ConvertToMilliseconds(duration)));
            }
            return listOfChartPoints;

        }
        public IList<Point> testGrapeAlg(int minSize, int maxSize, int step)
        {
            IList<Point> listOfChartPoints = new List<Point>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                //int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(size, size);
                factorB = matrGenerator.generateMatrix(size, size);
                int[,] result = new int[factorA.GetLength(0),factorB.GetLength(1)];
                int[] mulH = new int[factorA.GetLength(0)];
                int[] mulV = new int[factorB.GetLength(1)];
                DateTime start = DateTime.Now;
                MatrixMultiplication.GrapeAlg(ref result, factorA, factorB, ref mulH, ref mulV);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new Point(size, ConvertToMilliseconds(duration)));
            }
            return listOfChartPoints;


               
        }
        public IList<Point> testOpGrapeAlg(int minSize, int maxSize, int step)
        {
            IList<Point> listOfChartPoints = new List<Point>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
        //        int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(size, size);
                factorB = matrGenerator.generateMatrix(size,size);
                int[,] result = new int[factorA.GetLength(0),factorB.GetLength(1)];
                int[] mulH = new int[factorA.GetLength(0)];
                int[] mulV = new int[factorB.GetLength(1)];
                DateTime start = DateTime.Now;
                MatrixMultiplication.OpGrapeAlg(ref result, factorA, factorB, ref mulH, ref mulV);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new Point(size, ConvertToMilliseconds(duration)));
            }
            return listOfChartPoints;
        }
    }
}
