﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1;
using OxyPlot;

namespace Lab1
{
    public class Tester
    {
        private Matrix<int> factorA;
        private Matrix<int> factorB;
        private MatrixGenerator<int> matrGenerator; 
        public Tester()
        {
            matrGenerator = new MatrixGenerator<int>(new IntGenerator(0, 10));
//            factorA = matrGenerator.generateMatrix(3, 5);
//            factorB = matrGenerator.generateMatrix(5, 2);
        }
        public IList<DataPoint> testTraditional(int minSize, int maxSize, int step)
        {
            IList<DataPoint> listOfChartPoints = new List<DataPoint>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(numRowsInA, numColsInA);
                factorB = matrGenerator.generateMatrix(numColsInA, numRowsInA);
                Matrix<int> result = new Matrix<int>(factorA.RowsNum, factorB.ColumnsNum);
                DateTime start = DateTime.Now;
                MatrixMultiplication.Traditional(result, factorA, factorB);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new DataPoint(size, duration.Milliseconds));
            }
            return listOfChartPoints;

        }
        public IList<DataPoint> testGrapeAlg(int minSize, int maxSize, int step)
        {
            IList<DataPoint> listOfChartPoints = new List<DataPoint>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(numRowsInA, numColsInA);
                factorB = matrGenerator.generateMatrix(numColsInA, numRowsInA);
                Matrix<int> result = new Matrix<int>(factorA.RowsNum, factorB.ColumnsNum);
                Vector<int> mulH = new Vector<int>(factorA.RowsNum);
                Vector<int> mulV = new Vector<int>(factorB.ColumnsNum);
                DateTime start = DateTime.Now;
                MatrixMultiplication.GrapeAlg(result, factorA, factorB, mulH, mulV);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new DataPoint(size, duration.Milliseconds));
            }
            return listOfChartPoints;


               
        }
        public IList<DataPoint> testOpGrapeAlg(int minSize, int maxSize, int step)
        {
            IList<DataPoint> listOfChartPoints = new List<DataPoint>(); 
            for (var size = minSize; size <= maxSize; size+= step)
            {
                int numRowsInA = matrGenerator.generateRandDivider(size), numColsInA = size / numRowsInA;
                factorA = matrGenerator.generateMatrix(numRowsInA, numColsInA);
                factorB = matrGenerator.generateMatrix(numColsInA, numRowsInA);
                Matrix<int> result = new Matrix<int>(factorA.RowsNum, factorB.ColumnsNum);
                Vector<int> mulH = new Vector<int>(factorA.RowsNum);
                Vector<int> mulV = new Vector<int>(factorB.ColumnsNum);
                DateTime start = DateTime.Now;
                MatrixMultiplication.OpGrapeAlg(result, factorA, factorB, mulH, mulV);
                TimeSpan duration = DateTime.Now - start;
                listOfChartPoints.Add(new DataPoint(size, duration.Milliseconds));
            }
            return listOfChartPoints;
        }
    }
}
