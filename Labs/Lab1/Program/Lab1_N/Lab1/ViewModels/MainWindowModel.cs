﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Lab1.Annotations;

namespace Lab1.ViewModels
{
    public class MainWindowModel : INotifyPropertyChanged
    {
        private PlotModel plotModel;
        private Tester tester = new Tester();
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { plotModel = value; OnPropertyChanged("PlotModel"); }
        }

        private DateTime lastUpdate = DateTime.Now;

        public MainWindowModel()
        {
            PlotModel = new PlotModel();
            SetUpModel();
            LoadData();
        }

        private void SetUpModel()
        {
            PlotModel.LegendTitle = "Legend";
            PlotModel.LegendOrientation = LegendOrientation.Horizontal;
            PlotModel.LegendPlacement = LegendPlacement.Outside;
            PlotModel.LegendPosition = LegendPosition.TopRight;
            PlotModel.LegendBackground = OxyColor.FromAColor(200, OxyColors.White);
            PlotModel.LegendBorder = OxyColors.Black;
            var durationAxis = new LinearAxis(AxisPosition.Left, 0) { MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Duration" };
            PlotModel.Axes.Add(durationAxis);
            var sizeAxis = new LinearAxis(AxisPosition.Bottom, 0) { MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Title = "Size" };
            PlotModel.Axes.Add(sizeAxis);

        }
        private LineSeries createLineSeries(IList<DataPoint> listOfData, OxyColor markerColor, string title)
        {
                var lineSeries = new LineSeries
                {
                    ItemsSource = listOfData,
                    StrokeThickness = 2,
                    MarkerSize = 3,
                    MarkerStroke = markerColor,
//                    MarkerType = markerTypes[data.Key],
                    CanTrackerInterpolatePoints = false,
                    Title = title,
                    Smooth = false
                };
                return lineSeries;

        }
        private void LoadData()
        {
            int minSize = 100, maxSize = 1000, step = 100;
            IList<DataPoint> timeForTrad = tester.testTraditional(minSize, maxSize, step);
            IList<DataPoint> timeForGrape = tester.testGrapeAlg(minSize, maxSize, step);
            IList<DataPoint> timeForOpGrape = tester.testOpGrapeAlg(minSize, maxSize, step);
            var lineTr = createLineSeries(timeForTrad, OxyColors.Red, "Traditional algorithm");
            PlotModel.Series.Add(lineTr);
            var lineGr = createLineSeries(timeForGrape, OxyColors.Green, "Grape algorithm");
            PlotModel.Series.Add(lineGr);
        //    plotModel.Series.Add(createLineSeries(timeForOpGrape, OxyColors.Blue, "Optimized grape algorithm"));
        }

        public void UpdateModel()
        {
            /*
            list<measurement> measurements = data.getupdatedata(lastupdate);
            var dataperdetector = measurements.groupby(m => m.detectorid).orderby(m => m.key).tolist();

            foreach (var data in dataperdetector)
            {
                var lineserie = plotmodel.series[data.key] as lineseries;
                if (lineserie != null)
                {
                    data.tolist()
                        .foreach(d => lineserie.points.add(new datapoint(datetimeaxis.todouble(d.datetime), d.value)));
                }
            }
            lastupdate = datetime.now;
             */
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyname)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyname));
        }
    }
}
