﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lab7
{
    class Dispatcher
    {
        private int currCountOfJobs = 0;
        private int numOfThreads;
        private int numOfAllJobs;
        private Task[] tasks;
        private IList<Job> jobs;
        private IDictionary<Job, int[,]> resOfJobs;
        private CancellationTokenSource[] ctsArr;
        private int processTimeout = 4000;
        private readonly object consoleLock = new object();
        public Dispatcher(int numOfThreads, int numOfAllJobs, IList<Job> jobs, object consoleLock)
        {
            tasks = new Task[numOfThreads];
            ctsArr = Enumerable.Repeat(new CancellationTokenSource(), numOfThreads).ToArray(); 
            this.numOfThreads = numOfThreads;
            this.numOfAllJobs = numOfAllJobs;
            this.jobs = jobs;
            this.consoleLock = consoleLock;
            resOfJobs = new Dictionary<Job, int[,]>();
        }
        public Task RunMultiplier(Job job, int taskNum, IList<Job> jobs, CancellationTokenSource cts)
        {
            lock(consoleLock)
                Console.WriteLine("\tBegining of processing job #" + job.Id + ", job.Complexity = " + job.Complexity + ", thread #" + taskNum);
            var task = Task.Run(() =>
                {
                    Interlocked.Increment(ref numOfScheduledJobs);
                    var duration = job.Complexity / 1000;

                    var res = TraditionalMult.Mult(job.FactorA, job.FactorB);
                    if (job.ShouldWait)
                        Task.WaitAll(Task.Delay(duration));
                    resOfJobs[job] = res;
                }, cts.Token);
            return task.ContinueWith((continuation) => 
                {
                    var count = 0;
                    if (job.ShouldWait && job.Complexity / 1000 > processTimeout)
                    {
                        job.ShouldWait = false;
                        if (!jobs.Contains(job))
                        {

                            lock (jobs)
                                jobs.Add(job);
                            Interlocked.Decrement(ref numOfScheduledJobs);
                        }
                        lock (consoleLock)
                            Console.WriteLine("//////////// Thread #" + taskNum + " crashed, job.Id = " + job.Id + ", job.Complexity = " + job.Complexity); 
                    }
                    else
                    {
                        Interlocked.Increment(ref numOfCompletedJobs);
                        lock(consoleLock)
                            Console.WriteLine("\tEnd of processing of job #" + job.Id + ", job.Complexity = " + job.Complexity + ", thread #" + taskNum + (job.ShouldWait ? ", run" : ", rerun"));
                    }
                });
        }
        private int numOfScheduledJobs = 0;
        private int numOfCompletedJobs = 0;
        public void ProcessJobs()
        {
            while(numOfCompletedJobs < numOfAllJobs)
            {
                do
                {
                    lock (jobs)
                        currCountOfJobs = jobs.Count;
                }
                while (currCountOfJobs == 0 && numOfScheduledJobs <numOfAllJobs);
                Task.WaitAll(Task.Delay(processTimeout));
                for (var j = 0; j < tasks.Length; ++j)
                    if (tasks[j] == null || tasks[j].IsCompleted)
                    {
                        Job job = null;
                        lock (jobs)
                        {
                            var maxInd = GetJobWithMaxCompl(jobs);
                            if (maxInd != -1)
                            {
                                job = jobs[maxInd];
                                jobs.RemoveAt(maxInd);
                            }
                        }
                        if (job != null)
                        {
                            tasks[j] = RunMultiplier(job, j, jobs, ctsArr[j]);
                            lock (resOfJobs)
                                if (!resOfJobs.ContainsKey(job))
                                    resOfJobs.Add(job, null);
                        }
                    }
                if (!tasks.Contains(null))
                {
                    Task.WaitAll(tasks, processTimeout);
                    for(var i=0;i<tasks.Length;++i)
                        if (!tasks[i].IsCompleted)
                            ctsArr[i].Cancel();
                }
            }
        }
        public int GetJobWithMaxCompl(IList<Job> jobs)
        {
            int maxInd= -1; int maxCompl = 0; 
            for (var ind = 0; ind < jobs.Count; ++ind)
                if (maxCompl < jobs[ind].Complexity)
                {
                    maxInd = ind;
                    maxCompl = jobs[ind].Complexity;
                }
            return maxInd;
        }
    }
}
