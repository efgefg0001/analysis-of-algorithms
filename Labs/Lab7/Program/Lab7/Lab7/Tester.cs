﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Tester
    {
        private int minV, maxV;
        private int minS, maxS;
        private int maxDelay = 3000;
        private IList<Job> jobs = new List<Job>();
        private Random rand = new Random();
        private readonly object consoleLock = new object();
        public Tester(int minS, int maxS, int minV, int maxV)
        {
            this.minV = minV; this.maxV = maxV;
            this.minS = minS; this.maxS = maxS;
        }
        public Task RunJobGenerator(int numOfJobs)
        {
            return Task.Run(async () =>
                {
                    var generator = new JobGenerator(minS, maxS, minV, maxV);
                    for (var i = 0; i < numOfJobs; ++i)
                    {
                        var job = generator.GenerateJob();
                        await Task.Delay(rand.Next(1, maxDelay));
                        lock (jobs)
                        {
                            job.Id = i;
                            jobs.Add(job);
                            lock(consoleLock)
                                Console.WriteLine("Generation of job #" + i);
                        }
                    }
                });
        }
        public Task RunComputations(int numOfThreads, int numOfAllJobs)
        {
            return Task.Run(
                () => 
                {
                    var dispatcher = new Dispatcher(numOfThreads, numOfAllJobs, jobs, consoleLock);
                    dispatcher.ProcessJobs();
                }); 
        }
        public void Test(int numOfThreads, int numOfJobs)
        {
            Task.WaitAll(RunJobGenerator(numOfJobs),
                RunComputations(numOfThreads, numOfJobs));
        }
    }
}
