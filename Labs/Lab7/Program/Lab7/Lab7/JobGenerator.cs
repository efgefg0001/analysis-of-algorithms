﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lab7
{
    class JobGenerator
    {
        private int minSize, maxSize;
        private int minValue, maxValue;
        private Random rand = new Random();
        public JobGenerator(int minSize, int maxSize, int minValue, int maxValue)
        {
            this.minSize = minSize;
            this.maxSize = maxSize;
            this.minValue = minValue;
            this.maxValue = maxValue;
        }
        private int[,] GenerateMatrix(int size)
        {
            var matrix = new int[size, size];
            var numRow = matrix.GetLength(0);
            var numCol = matrix.GetLength(1);
            for (var i = 0; i < numRow; ++i)
                for (var j = 0; j < numCol; ++j)
                    matrix[i, j] = rand.Next(minValue, maxValue);
            return matrix;
        }
        public Job GenerateJob()
        {
            var job = new Job(new TraditionalMult());
            var size = rand.Next(minSize, maxSize);
            job.FactorA = GenerateMatrix(size);
            job.FactorB = GenerateMatrix(size);
            return job;
        }
    }
}
