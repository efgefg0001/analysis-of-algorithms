﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTh = 4, numOfJobs = 10;
            var tester = new Tester(50, 100, 1, 20);
            tester.Test(numOfTh, numOfJobs);
        }
    }
}
