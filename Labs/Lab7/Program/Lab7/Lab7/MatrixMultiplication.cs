﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class TraditionalMult : IMatrixMultiplication 
    {
        public TraditionalMult() { }
        /*
        public static bool CanMultiply<Type>(Type[,] factorA, Type[,] factorB)
        {
            return factorA.GetLength(0) == factorB.GetLength(1) ? true : false;
        }
        */ 
        int[,] IMatrixMultiplication.Multiply(int[,] factorA, int[,] factorB)
        {
            int m = factorA.GetLength(0);
            int n = factorA.GetLength(1);
            int q = factorB.GetLength(1);
            var result = new int[m,q];
            for (var i = 0; i < m; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = 0;
                    for (var k = 0; k < n; ++k)
                        result[i, j] += factorA[i, k] * factorB[k, j];
                }
            }
            return result;
        }
        public static int[,] Mult(int[,] factorA, int[,] factorB)
        {
            int m = factorA.GetLength(0);
            int n = factorA.GetLength(1);
            int q = factorB.GetLength(1);
            var result = new int[m,q];
            for (var i = 0; i < m; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = 0;
                    for (var k = 0; k < n; ++k)
                        result[i, j] += factorA[i, k] * factorB[k, j];
                }
            }
            return result;
        }
        int IMatrixMultiplication.Complexity(int numRowA, int numColA, int numColB)
        {
            return 10*numRowA*numColA*numColB+7*numRowA*numColB+4*numRowA+5;
        }
/*


        public static void GrapeAlg(ref int[,] result, int[,] factorA, int[,] factorB, ref int[] mulH, ref int[] mulV)
        {
            int m = factorA.GetLength(0), n = factorA.GetLength(1), q = factorB.GetLength(1);
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < n / 2; ++j)
                    mulH[i] += factorA[i, j * 2] * factorA[i, j * 2 + 1];
            for (var i = 0; i < q; ++i)
                for (var j = 0; j < n / 2; ++j)
                    mulV[i] += factorB[j * 2, i] * factorB[j * 2 + 1, i];
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = -mulH[i] - mulV[j];
                    for (var k = 0; k < n / 2; ++k)
                        result[i, j] += (factorA[i, k * 2] + factorB[k * 2 + 1, j]) * (factorA[i, k * 2 + 1] + factorB[k * 2, j]);
                }
            if(n % 2 == 1)
            {
                for (var i = 0; i < m; ++i)
                    for (var j = 0; j < q; ++j)
                        result[i, j] += factorA[i, n - 1] * factorB[n - 1, j];
            }
        }

        public static void OpGrapeAlg(ref int[,] result, int[,] factorA, int[,] factorB, 
            ref int[] mulH, ref int[] mulV)
        {
            int m = factorA.GetLength(0), n = factorA.GetLength(1), q = factorB.GetLength(1);
            bool flag = n % 2 == 1;
            int idx = n - 1;
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulH[i] -= factorA[i, j] * factorA[i, j + 1];
            for (var i = 0; i < q; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulV[i] -= factorB[j, i] * factorB[j + 1, i];
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] += mulH[i] + mulV[j];
                    for (var k = 0; k < idx; k += 2)
                        result[i, j] += (factorA[i, k] + factorB[k + 1, j]) * (factorA[i, k + 1] + factorB[k, j]);
                    if (flag)
                        result[i, j] += factorA[i, idx] * factorB[idx, j];
                }
 
        }
    */

    }
}
