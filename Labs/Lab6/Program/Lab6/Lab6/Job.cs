﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    class Job
    {
        public Job(IMatrixMultiplication mult)
        {
            Mult = mult;
        }
        public int Id { get; set; }
        public override int GetHashCode()
        {
            return Id; 
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode(); 
        }
        public int[,] FactorA { get; set; }
        public int[,] FactorB { get; set; }
        public IMatrixMultiplication Mult { get; private set; }
        public int[,] Result 
        {
            get
            {
                return Mult.Multiply(FactorA, FactorB);
            }
        }
        public int Complexity
        {
            get
            {
                var numRowA = FactorA.GetLength(0);
                var numColA = FactorA.GetLength(1); 
                var numColB = FactorB.GetLength(1);
                return Mult.Complexity(numRowA, numColA, numColB); 
            }
        }
    }
}
