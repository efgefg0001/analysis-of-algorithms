﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lab6
{
    class Dispatcher
    {
        private ManualResetEvent[] waitHandles;
        private int currCountOfJobs = 0;
        private int numOfThreads;
        private int numOfAllJobs;
        private Task[] tasks;
        private IList<Job> jobs;
        private IDictionary<Job, int[,]> resOfJobs;
        private int processTimeout = 7000;
        public Dispatcher(int numOfThreads, int numOfAllJobs, IList<Job> jobs)
        {
            tasks = new Task[numOfThreads];
            this.numOfThreads = numOfThreads;
            this.numOfAllJobs = numOfAllJobs;
            this.jobs = jobs;
            resOfJobs = new Dictionary<Job, int[,]>();
        }
        public Task RunMultiplier(Job job, int taskNum)
        {
            Console.WriteLine("\tBegining of processing job #" + job.Id + ", job.Complexity = " + job.Complexity + ", thread #" + taskNum);
            return Task.Run(() =>
                {
                    var res = TraditionalMult.Mult(job.FactorA, job.FactorB);
                    Task.WaitAll(Task.Delay(job.Complexity/1000));
                    resOfJobs[job] = res;
//                    Console.WriteLine("\tEnd of processing of job #" + job.Id + ", job.Complexity = " + job.Complexity + ", thread #" + taskNum);
                });
        }
        public void ProcessJobs()
        {
            for (var i = 0; i < numOfAllJobs; )
            {
                do
                {
                    lock (jobs)
                        currCountOfJobs = jobs.Count;
                }
                while (currCountOfJobs == 0);
                Task.WaitAll(Task.Delay(processTimeout));
                for (var j = 0; j < tasks.Length; ++j)
                    if (tasks[j] == null || tasks[j].IsCompleted)
                    {
                        Job job = null;
                        lock (jobs)
                        {
                            var maxInd = GetJobWithMaxCompl(jobs);
                            if (maxInd != -1)
                            {
                                job = jobs[maxInd];
                                jobs.RemoveAt(maxInd);
                            }
                        }
                        if (job != null)
                        {
                            tasks[j] = RunMultiplier(job, j);
                            ++i;
                            lock (resOfJobs)
                                if (!resOfJobs.ContainsKey(job))
                                    resOfJobs.Add(job, null);
                        }
                    }
                if (!tasks.Contains(null))
                    Task.WaitAny(tasks);
            }
            Task.WaitAll(tasks);
        }
        public int GetJobWithMaxCompl(IList<Job> jobs)
        {
            int maxInd= -1; int maxCompl = 0; 
            for (var ind = 0; ind < jobs.Count; ++ind)
                if (maxCompl < jobs[ind].Complexity)
                {
                    maxInd = ind;
                    maxCompl = jobs[ind].Complexity;
                }
            return maxInd;
        }
    }
}
