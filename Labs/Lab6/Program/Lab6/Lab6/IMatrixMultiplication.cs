﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    interface IMatrixMultiplication
    {
        int[,] Multiply(int[,] fA, int[,] fB);
        int Complexity(int m, int n, int k);
    }
}
