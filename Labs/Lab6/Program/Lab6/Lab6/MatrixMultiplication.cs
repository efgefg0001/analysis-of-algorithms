﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    class TraditionalMult : IMatrixMultiplication 
    {
        public TraditionalMult() { }

        int[,] IMatrixMultiplication.Multiply(int[,] factorA, int[,] factorB)
        {
            int m = factorA.GetLength(0);
            int n = factorA.GetLength(1);
            int q = factorB.GetLength(1);
            var result = new int[m,q];
            for (var i = 0; i < m; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = 0;
                    for (var k = 0; k < n; ++k)
                        result[i, j] += factorA[i, k] * factorB[k, j];
                }
            }
            return result;
        }
        public static int[,] Mult(int[,] factorA, int[,] factorB)
        {
            int m = factorA.GetLength(0);
            int n = factorA.GetLength(1);
            int q = factorB.GetLength(1);
            var result = new int[m,q];
            for (var i = 0; i < m; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = 0;
                    for (var k = 0; k < n; ++k)
                        result[i, j] += factorA[i, k] * factorB[k, j];
                }
            }
            return result;
        }
        int IMatrixMultiplication.Complexity(int numRowA, int numColA, int numColB)
        {
            return 10*numRowA*numColA*numColB+7*numRowA*numColB+4*numRowA+5;
        }


    }
}
