﻿namespace AlgalLab01
{
    public static class Sorter
    {
        private static void Swap(int[] array, int index1, int index2) 
        {
	        int temp = array[index1];           
	        array[index1] = array[index2];      
	        array[index2] = temp;               
        }

        public static void Insert(int[] list)
        {
            int n = list.Length;
            for (int i = 1; i < n; i++)
            {
                int j = i;
                while (j > 0 && list[j] < list[j - 1])
                {
                    Swap(list, j, j-1);
                    --j;
                }
            }
        }

        public static void  ShakerSort(int[] list)
        {
            int Left = 0, Right = list.Length-1; //границы сортировки
            do
            {
                for (int i = Right; i > Left; --i)
                {
                    if (list[i - 1]>list[i])
                    {
                        Swap(list, i, i-1);                
                    }
                }  
 
                ++Left;
                //Сдвигаем к концу массива "тяжелые элементы"
                for (int i = Left; i <= Right; i++)
                {
                    if (list[i - 1] > list[i])
                    {
                        Swap(list, i, i-1);
                    }
                }
                --Right;
            }
            while (Left < Right);
        }
    }
}

