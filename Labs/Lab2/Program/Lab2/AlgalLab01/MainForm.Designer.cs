﻿namespace AlgalLab01
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bestCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.theWorstCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Graph
            // 
            chartArea5.Name = "ChartArea1";
            this.Graph.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.Graph.Legends.Add(legend5);
            this.Graph.Location = new System.Drawing.Point(12, 27);
            this.Graph.Name = "Graph";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series9.Color = System.Drawing.Color.Red;
            series9.Legend = "Legend1";
            series9.Name = "Insert";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series10.Color = System.Drawing.Color.Navy;
            series10.Legend = "Legend1";
            series10.Name = "Shaker";
            this.Graph.Series.Add(series9);
            this.Graph.Series.Add(series10);
            this.Graph.Size = new System.Drawing.Size(1346, 561);
            this.Graph.TabIndex = 0;
            this.Graph.Text = "chart1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.bestCaseToolStripMenuItem,
            this.theWorstCaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1370, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.startToolStripMenuItem.Text = "Random numbers";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // bestCaseToolStripMenuItem
            // 
            this.bestCaseToolStripMenuItem.Name = "bestCaseToolStripMenuItem";
            this.bestCaseToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.bestCaseToolStripMenuItem.Text = "The Best case";
            this.bestCaseToolStripMenuItem.Click += new System.EventHandler(this.bestCaseToolStripMenuItem_Click);
            // 
            // theWorstCaseToolStripMenuItem
            // 
            this.theWorstCaseToolStripMenuItem.Name = "theWorstCaseToolStripMenuItem";
            this.theWorstCaseToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.theWorstCaseToolStripMenuItem.Text = "The worst case";
            this.theWorstCaseToolStripMenuItem.Click += new System.EventHandler(this.theWorstCaseToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 600);
            this.Controls.Add(this.Graph);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Sorts";
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Graph;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bestCaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theWorstCaseToolStripMenuItem;
    }
}

