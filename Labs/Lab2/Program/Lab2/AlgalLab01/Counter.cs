﻿using System;
using System.Collections.Generic;

namespace AlgalLab01
{
    public delegate int[] GenerateArrayDelegate(int n);
    public class Counter
    {
        public Dictionary<int, int> Insert { get; private set; }

        public Dictionary<int, int> Shaker { get; private set; }


        public Counter()
        {
            Insert = new Dictionary<int, int>();
            Shaker = new Dictionary<int, int>();
        }

        public void MeasureTime(GenerateArrayDelegate generate)
        {
            const int step = 100;
            for (int i = 100; i <= 1000; i += step)
            {
                Insert.Add(i, MeasureTime(i, Sorter.Insert, generate));
                Shaker.Add(i, MeasureTime(i, Sorter.ShakerSort, generate));
            }
        }

        public static int[] GenerateArray(int n)
        {
            int[] arr = new int[n];
            Random rand = new Random();

            for (int i = 0; i < n; ++i)
                arr[i] = rand.Next(int.MaxValue);
            return arr;
        }
        public static int[] BestCase(int n)
        {
            int[] arr = new int[n];
            for (int i = 0; i < n; ++i)
                arr[i] = i;
            return arr;
        }
        public static int[] WorstCase(int n)
        {
            int[] arr = new int[n];
            int maxInd = n - 1;
            for (var i = 0; i < n; ++i)
                arr[i] = maxInd - i;
            return arr;
        }
        private int getMilliseconds(TimeSpan duration)
        {
            int result = duration.Milliseconds + (duration.Seconds + duration.Minutes * 60)*1000;
            return result;
        }
        public int MeasureTime(int i, Action<int[]> action, GenerateArrayDelegate generate)
        {
            int numOfExp = 100;
            int[][] arr = new int[numOfExp][];
            for(int j = 0; j<100; ++j)
            {
                arr[j] = generate(i);
            }
            var start = DateTime.Now;
            foreach (var array in arr)
            {
                action(array);
            }
            var stop = DateTime.Now;
            TimeSpan duration = stop - start;
            return getMilliseconds(duration); 
        }
    }
}
