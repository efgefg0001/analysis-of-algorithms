﻿using System;
using System.Windows.Forms;

namespace AlgalLab01
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graph.Series[0].Points.Clear();
            Graph.Series[1].Points.Clear();
            var counter = new Counter();
            counter.MeasureTime(Counter.GenerateArray);

            foreach (var key in counter.Insert.Keys)
            {
                Graph.Series[0].Points.AddXY(key, counter.Insert[key]);
                Graph.Series[1].Points.AddXY(key, counter.Shaker[key]);
            }
        }

        private void bestCaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graph.Series[0].Points.Clear();
            Graph.Series[1].Points.Clear();
            var counter = new Counter();
            counter.MeasureTime(Counter.BestCase);

            foreach (var key in counter.Insert.Keys)
            {
                Graph.Series[0].Points.AddXY(key, counter.Insert[key]);
                Graph.Series[1].Points.AddXY(key, counter.Shaker[key]);
            }
        }

        private void theWorstCaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Graph.Series[0].Points.Clear();
            Graph.Series[1].Points.Clear();
            var counter = new Counter();
            counter.MeasureTime(Counter.WorstCase);

            foreach (var key in counter.Insert.Keys)
            {
                Graph.Series[0].Points.AddXY(key, counter.Insert[key]);
                Graph.Series[1].Points.AddXY(key, counter.Shaker[key]);
            }
        }
    }
}
