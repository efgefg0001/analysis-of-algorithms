﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Program
    {
        static Program()
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Слова {0} и {1}, расстояние Левенштейна = {2}", "aunt", "ant", LevensteinDistance.Compute("aunt", "ant"));
            Console.WriteLine("Слова {0} и {1}, расстояние Левенштейна = {2}", "Sam", "Samantha", LevensteinDistance.Compute("Sam", "Samantha"));
            Console.WriteLine("Слова {0} и {1}, расстояние Левенштейна = {2}", "flomax", "volmax", LevensteinDistance.Compute("flomax", "volmax"));
        }
    }
}
