﻿using System;

namespace Lab4
{
    class Program
    {

        static void Main(string[] args)
        {
            Tester tester = new Tester();
            tester.TestSubstringAlg(SubstringFinder.StandartAlg, "Standart algorithm");
            Console.ReadKey();
            tester.TestSubstringAlg(SubstringFinder.KnuthMorrisPrattAlg, "Knuth–Morris–Pratt algorithm");
            Console.ReadKey();
        }
    }
}
