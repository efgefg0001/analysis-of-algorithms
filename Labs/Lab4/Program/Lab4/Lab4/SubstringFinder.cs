﻿using System;

namespace Lab4
{
    class SubstringFinder
    {
        public static Tuple<int, int> StandartAlg(string str, string substr)
        {
            int substrLoc = 0, strLoc = substrLoc;
            int numOfComparisons = 0;
            while(strLoc < str.Length && substrLoc < substr.Length)
                if (str[strLoc] == substr[substrLoc])
                {
                    ++substrLoc;
                    ++strLoc;
                    ++numOfComparisons;
                }
                else
                {
                    strLoc = strLoc - substrLoc + 1;
                    substrLoc = 0;
                    ++numOfComparisons;
                }
            return new Tuple<int, int>
            (
                substrLoc >= substr.Length ? strLoc - substrLoc: -1,
                numOfComparisons
            );
        }
        private static int[] getFailArr(string substr)
        {
            int[] fail = new int[substr.Length];
            fail[0] = -1;
            for (var i = 1; i < substr.Length; ++i)
            {
                var temp = fail[i - 1];
                while (temp > 0 && substr[temp] != substr[i - 1])
                    temp = fail[temp];
                fail[i] = temp + 1;
            }
            return fail;
        }
        public static Tuple<int, int> KnuthMorrisPrattAlg(string str, string substr)
        {
            int substrLoc = 0, strLoc = substrLoc;
            int numOfComparisons = 0;
            int[] fail = getFailArr(substr);
            while (strLoc < str.Length && substrLoc < substr.Length)
            {

                if (substrLoc == -1 || str[strLoc] == substr[substrLoc])
                {
                    if(!(substrLoc == -1))
                        ++numOfComparisons;
                    ++strLoc;
                    ++substrLoc;
                }
                else
                {
                    ++numOfComparisons;
                    substrLoc = fail[substrLoc];
                }
            }
            return new Tuple<int, int>
            (
                substrLoc >= substr.Length ? strLoc - substrLoc: -1,
                numOfComparisons
            );            
        }
    }
}
