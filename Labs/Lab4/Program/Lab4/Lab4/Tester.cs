﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    delegate Tuple<int,int> TestSubstringAlgDelegate(string str, string substr);
    class Tester
    {
        private string[] strings = {
            "C# is intended to be a simple, modern, general-purpose, object-oriented programming language.", 
            "adddsssabababc--"
        };
        private string[] substrings = {
            "functional programming",
            "C# is",
            "language.",
            "to be a simple, modern, general-purpose, object-oriented programming language.",
            "D# is intended to be a simple, modern, general-purpose, object-oriented programming language.",
            "C+ is intended to be a simple, modern, general-purpose, object-oriented programming language.", 
            "C# is intended to be a simple, modern, general-purpose, object-oriented programming languagg.",   
            "C# is intended to be a simple, modern, general-purpose, object-oriented programming language!",
            "ababc",
            "ababac"

        };
        private void RunOneTest(TestSubstringAlgDelegate substrMethod, string testDescription,
            string str, string substr)
        {
            Console.WriteLine(testDescription);
            Console.WriteLine("String = \"" + str + "\"");
            Console.WriteLine("Substring = \"" + substr + "\"");
            var res = substrMethod(str, substr);
            Console.WriteLine("The position of a substring in a string = " + res.Item1);
            Console.WriteLine("The number of comparisons = " + res.Item2);
        }
        public void TestSubstringAlg(TestSubstringAlgDelegate substrMethod, string algName)
        {
            Console.WriteLine(algName);
            RunOneTest(substrMethod,
               "1.The absence of a substring in a string (worst case)",
               strings[0], substrings[0]
            );
            RunOneTest(substrMethod,
                "2.Occurrence of the substring in the source string at the beginning",
                strings[0], substrings[1]
            );
            RunOneTest(substrMethod,
                "3.Occurrence of the substring in the source string at the end",
                strings[0], substrings[2]
            );
            RunOneTest(substrMethod,
                "4.The matching substring for the i-th position of the string",
                strings[0], substrings[3]
            );
            RunOneTest(substrMethod,
                "5.The mismatching in the 0th character of the substring", //first, the second, penultimate, the last character of the substring",
                strings[0], substrings[4]
            );
            RunOneTest(substrMethod,
                "6.The mismatching in the 1th character of the substring", //first, the second, penultimate, the last character of the substring",
                strings[0], substrings[5]
            );
            RunOneTest(substrMethod,
                "7.The mismatching in the penultimate character of the substring", //first, the second, penultimate, the last character of the substring",
                strings[0], substrings[6]
            );
            RunOneTest(substrMethod,
                "8.The mismatching in the last character of the substring", //first, the second, penultimate, the last character of the substring",
                strings[0], substrings[7]
            );
            RunOneTest(substrMethod,
                "9.The matching on the repeated subsequences",
                strings[1], substrings[8]
            );
            RunOneTest(substrMethod,
                "10.The mismatching on the repeated subsequences",
                strings[1], substrings[9]                
            );
            /*
            RunOneTest(substrMethod,
                "11. Last test",
                "str", "substr" 
            );
            */
            Console.WriteLine("------------------------------------\n");
        }
    }
}
