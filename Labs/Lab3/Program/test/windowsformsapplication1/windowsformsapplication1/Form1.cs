﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ConsoleApplication1;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        private void TimeAnalysys(DataPointCollection pnts1, DataPointCollection pnts2, DataPointCollection pnts3,
            DataPointCollection pnts4, DataPointCollection pnts5, DataPointCollection pnts6, DataPointCollection pnts7)
        {
            for (var n = 100; n <= 1000; n += 100)
            {
                var matrices = new GenMatrix(n);
                int time;
                time = matrices.Multiply(16);
                pnts7.AddXY(n, time);

                time = matrices.Multiply();
                pnts6.AddXY(n, time);

                time = matrices.Multiply(1);
                pnts1.AddXY(n, time);

                time = matrices.Multiply(2);
                pnts2.AddXY(n, time);

                time = matrices.Multiply(4);
                pnts3.AddXY(n, time);

                time = matrices.Multiply(8);
                pnts4.AddXY(n, time);

                time = matrices.MultiplyVinograd();
                pnts5.AddXY(n, time);
            }
        }

        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            TimeAnalysys(chart1.Series["Series1"].Points, chart1.Series["Series2"].Points, chart1.Series["Series3"].Points, chart1.Series["Series4"].Points,
                chart1.Series["Series5"].Points, chart1.Series["Series6"].Points, chart1.Series["Series7"].Points);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
