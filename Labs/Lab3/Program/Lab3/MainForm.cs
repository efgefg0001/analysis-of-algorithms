﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Matrix
{
    public partial class MainForm : Form
    {
        private TesterMatrDelegate testerDelegate;
        private Tester tester = new Tester(0, 10);
        public MainForm()
        {
            InitializeComponent();
            ordinaryAlgRdBtn.Checked = true;
            testerDelegate = tester.testAllTraditional; 
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
//            TimeAnalysys(MatrixMultiplier.OrdinaryAlgorythm, MatrixMultiplier.VinogradAlgorythm, MatrixMultiplier.VinogradAlgorythmModificated,
//                chart1.Series["Ordinary"].Points, chart1.Series["Vinograd"].Points, chart1.Series["Vinograd_mod"].Points);
        }
        private async Task DrawAllCharts(int min, int max, int step)
        {
            for(var size = min; size <= max; size += step)
            {
                Point[] points = await testerDelegate(size);
                DrawOneColumn(points);
            }

        }
        private void DrawOneColumn(Point[] points)
        {
            for (var i = 0; i < points.Length; ++i)
                chart1.Series[i].Points.AddXY(points[i].X, points[i].Y);
        }
        private void CleanSeries()
        {
            foreach (var series in chart1.Series)
                series.Points.Clear();
        }
        private async void analyseButton_Click(object sender, EventArgs e)
        {
            try
            {
                CleanSeries();
                int minSize = int.Parse(minTextBox.Text);
                int maxSize = int.Parse(maxTextBox.Text);
                int step = int.Parse(stepTextBox.Text);
                await DrawAllCharts(minSize, maxSize, step);
//                DrawChart(1, tester.testGrapeAlg(minSize, maxSize, step));
//                DrawChart(2, tester.testOpGrapeAlg(minSize, maxSize, step));
            }
            catch(Exception error)
            {
                MessageBox.Show(error.ToString());
            }
        }

        private void ordinaryAlgRdBtn_Click(object sender, EventArgs e)
        {
            testerDelegate = tester.testAllTraditional; 
        }

        private void grapeAlgRdBtn_Click(object sender, EventArgs e)
        {
            testerDelegate = tester.testAllOpGrapeAlg;
        }
    }
}
