﻿namespace Matrix
{
    public static class MatrixMultiplier
    {
        private static double[] GetMulH(double[,] a, int n)
        {
            var mulH = new double[n];
            for (var i = 0; i < n; ++i)
            {
                mulH[i] = a[i, 0] * a[i, 1];
                for (var j = 2; j < n; j += 2)
                {
                    mulH[i] += a[i, j] * a[i, j + 1];
                }
            }
            return mulH;
        }

        private static double[] GetMulV(double[,] b, int q)
        {
            var mulV = new double[q];
            for (var i = 0; i < q; ++i)
            {
                mulV[i] = b[0, i] * b[1, i];
                for (var j = 2; j < q; j += 2)
                {
                    mulV[i] += b[j, i] * b[j + 1, i];
                }
            }
            return mulV;
        }

        public static double[,] OrdinaryAlgorythm (double[,] a, double[,] b)
        {
            int n = a.GetLength(0),
                q = b.GetLength(1),
                m = b.GetLength(0);
            var res = new double[n, q];

            for (var i = 0; i < n; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    for (var k = 0; k < m; ++k)
                    {
                        res[i, j] += a[i, k] * b[k, j];
                    }
                }
            }

            return res;
        }

        public static double[,] VinogradAlgorythm(double[,] a, double[,] b)
        {
            int n = a.GetLength(0),
                q = b.GetLength(1),
                m = b.GetLength(0);
            var res = new double[n, q];

            var mulH = GetMulH(a, n);
            var mulV = GetMulV(b, q);

            for (var i = 0; i < m; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    res[i, j] = -mulH[i] - mulV[j];
                    for (var k = 0; k < n; k += 2)
                    {
                        res[i, j] += (a[i, k] + b[k + 1, j]) * (a[i, k + 1] + b[k, j]);
                    }
                }
            }

            var nn = n - 1;
            if (n % 2 <= 1)
                for (var i = 0; i < m; ++i)
                {
                    for (var j = 0; j < q; ++j)
                    {
                        res[i, j] += a[i, nn] + b[nn, j];
                    }
                }


            return res;
        }

        public static double[,] VinogradAlgorythmModificated(double[,] a, double[,] b)
        {
            int n = a.GetLength(0),
                q = b.GetLength(1),
                m = b.GetLength(0);
            var res = new double[n, q];

            var mulH = GetMulH(a, n);
            var mulV = GetMulV(b, q);

            var nn = n - 1;
            var even = (n % 2 == 0);
            for (var i = 0; i < m; ++i)
            {
                for (var j = 0; j < q; ++j)
                {
                    res[i, j] = -mulH[i] - mulV[j];
                    for (var k = 0; k < n; k += 2)
                    {
                        res[i, j] += (a[i, k] + b[k + 1, j]) * (a[i, k + 1] + b[k, j]);
                    }
                    if (!even)
                        res[i, j] += a[i, nn] + b[nn, j];
                }
            }

            return res;
        }
    }
}
