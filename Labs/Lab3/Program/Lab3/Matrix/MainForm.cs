﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Matrix
{
    public partial class MainForm : Form
    {
        private delegate double[,] MatrixesMultAlgorithm(double[,] a, double[,] b);

        private double[,] FillMatr(int n, int m)
        {
            var res = new double[n, m];
            var rand = new Random();

            for (var i = 0; i > n; ++i)
            {
                for (var j = 0; j < m; ++j)
                {
                    res[i, j] = rand.Next(100) * rand.NextDouble();
                }
            }

            return res;
        }

        private void TimeAnalysys(MatrixesMultAlgorithm alg1, MatrixesMultAlgorithm alg2, MatrixesMultAlgorithm alg3, 
            DataPointCollection pnts1, DataPointCollection pnts2, DataPointCollection pnts3)
        {
            for (var n = 100; n <= 600; n += 100)
            {
                var m1 = FillMatr(n, n);
                var m2 = FillMatr(n, n);

                var start = DateTime.Now;
                var m3 = alg1(m1, m2);
                var stop = DateTime.Now;
                var result = stop - start;
//                var time = /*result.Minutes*60.0 + result.Seconds*1000.0 + */1.0 * result.Milliseconds;
                var time = (result.Minutes*60.0 + result.Seconds)*1000.0 + result.Milliseconds;
                pnts1.AddXY(n, time);

                start = DateTime.Now;
                m3 = alg2(m1, m2);
                stop = DateTime.Now;
                result = stop - start;
//                time = /*result.Minutes*60.0 + result.Seconds*1000.0 + */1.0 * result.Milliseconds;
                time = (result.Minutes * 60.0 + result.Seconds) * 1000.0 + result.Milliseconds;
                pnts2.AddXY(n, time);

                start = DateTime.Now;
                m3 = alg3(m1, m2);
                stop = DateTime.Now;
                result = stop - start;
//                time = /*result.Minutes*60.0 + result.Seconds*1000.0 + */1.0 * result.Milliseconds;
                time = (result.Minutes * 60.0 + result.Seconds) * 1000.0 + result.Milliseconds;
                pnts3.AddXY(n, time);
            }
        }

        public MainForm()
        {
            InitializeComponent();
            
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            TimeAnalysys(MatrixMultiplier.OrdinaryAlgorythm, MatrixMultiplier.VinogradAlgorythm, MatrixMultiplier.VinogradAlgorythmModificated,
                chart1.Series["Ordinary"].Points, chart1.Series["Vinograd"].Points, chart1.Series["Vinograd_mod"].Points);
        }
    }
}
