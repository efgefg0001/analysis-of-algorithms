﻿namespace Matrix
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.analyseButton = new System.Windows.Forms.Button();
            this.minTextBox = new System.Windows.Forms.TextBox();
            this.maxTextBox = new System.Windows.Forms.TextBox();
            this.stepTextBox = new System.Windows.Forms.TextBox();
            this.minLabel = new System.Windows.Forms.Label();
            this.maxLabel = new System.Windows.Forms.Label();
            this.stepLabel = new System.Windows.Forms.Label();
            this.controlPanel = new System.Windows.Forms.Panel();
            this.chooseAlgGrpBx = new System.Windows.Forms.GroupBox();
            this.grapeAlgRdBtn = new System.Windows.Forms.RadioButton();
            this.ordinaryAlgRdBtn = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.controlPanel.SuspendLayout();
            this.chooseAlgGrpBx.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.Name = "Canvas";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(-2, -1);
            this.chart1.Name = "chart1";
            series1.ChartArea = "Canvas";
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.LegendText = "Без многопоточности";
            series1.Name = "WithoutMultyThreading";
            series2.ChartArea = "Canvas";
            series2.Color = System.Drawing.Color.Green;
            series2.Legend = "Legend1";
            series2.LegendText = "1 поток";
            series2.Name = "Thread1";
            series3.ChartArea = "Canvas";
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            series3.Legend = "Legend1";
            series3.LegendText = "2 потока";
            series3.Name = "Thread2";
            series4.ChartArea = "Canvas";
            series4.Legend = "Legend1";
            series4.LegendText = "4 потока";
            series4.Name = "Thread4";
            series5.ChartArea = "Canvas";
            series5.Legend = "Legend1";
            series5.LegendText = "8 потоков";
            series5.Name = "Thread8";
            series6.ChartArea = "Canvas";
            series6.Color = System.Drawing.Color.Gray;
            series6.Legend = "Legend1";
            series6.LegendText = "16 потоков";
            series6.Name = "Thread16";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Size = new System.Drawing.Size(1040, 750);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // analyseButton
            // 
            this.analyseButton.Location = new System.Drawing.Point(69, 139);
            this.analyseButton.Name = "analyseButton";
            this.analyseButton.Size = new System.Drawing.Size(101, 23);
            this.analyseButton.TabIndex = 1;
            this.analyseButton.Text = "Построить";
            this.analyseButton.UseVisualStyleBackColor = true;
            this.analyseButton.Click += new System.EventHandler(this.analyseButton_Click);
            // 
            // minTextBox
            // 
            this.minTextBox.Location = new System.Drawing.Point(124, 17);
            this.minTextBox.Name = "minTextBox";
            this.minTextBox.Size = new System.Drawing.Size(100, 20);
            this.minTextBox.TabIndex = 2;
            // 
            // maxTextBox
            // 
            this.maxTextBox.Location = new System.Drawing.Point(124, 53);
            this.maxTextBox.Name = "maxTextBox";
            this.maxTextBox.Size = new System.Drawing.Size(100, 20);
            this.maxTextBox.TabIndex = 3;
            // 
            // stepTextBox
            // 
            this.stepTextBox.Location = new System.Drawing.Point(124, 89);
            this.stepTextBox.Name = "stepTextBox";
            this.stepTextBox.Size = new System.Drawing.Size(100, 20);
            this.stepTextBox.TabIndex = 4;
            // 
            // minLabel
            // 
            this.minLabel.AutoSize = true;
            this.minLabel.Location = new System.Drawing.Point(19, 17);
            this.minLabel.Name = "minLabel";
            this.minLabel.Size = new System.Drawing.Size(83, 26);
            this.minLabel.TabIndex = 5;
            this.minLabel.Text = "Минимальный \r\nразмер";
            // 
            // maxLabel
            // 
            this.maxLabel.AutoSize = true;
            this.maxLabel.Location = new System.Drawing.Point(19, 53);
            this.maxLabel.Name = "maxLabel";
            this.maxLabel.Size = new System.Drawing.Size(86, 26);
            this.maxLabel.TabIndex = 6;
            this.maxLabel.Text = "Максимальный\r\nразмер";
            // 
            // stepLabel
            // 
            this.stepLabel.AutoSize = true;
            this.stepLabel.Location = new System.Drawing.Point(75, 96);
            this.stepLabel.Name = "stepLabel";
            this.stepLabel.Size = new System.Drawing.Size(27, 13);
            this.stepLabel.TabIndex = 7;
            this.stepLabel.Text = "Шаг";
            // 
            // controlPanel
            // 
            this.controlPanel.Controls.Add(this.analyseButton);
            this.controlPanel.Controls.Add(this.stepLabel);
            this.controlPanel.Controls.Add(this.minTextBox);
            this.controlPanel.Controls.Add(this.stepTextBox);
            this.controlPanel.Controls.Add(this.maxLabel);
            this.controlPanel.Controls.Add(this.minLabel);
            this.controlPanel.Controls.Add(this.maxTextBox);
            this.controlPanel.Location = new System.Drawing.Point(1044, 73);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Size = new System.Drawing.Size(238, 174);
            this.controlPanel.TabIndex = 8;
            // 
            // chooseAlgGrpBx
            // 
            this.chooseAlgGrpBx.Controls.Add(this.grapeAlgRdBtn);
            this.chooseAlgGrpBx.Controls.Add(this.ordinaryAlgRdBtn);
            this.chooseAlgGrpBx.Location = new System.Drawing.Point(1044, 12);
            this.chooseAlgGrpBx.Name = "chooseAlgGrpBx";
            this.chooseAlgGrpBx.Size = new System.Drawing.Size(238, 55);
            this.chooseAlgGrpBx.TabIndex = 9;
            this.chooseAlgGrpBx.TabStop = false;
            this.chooseAlgGrpBx.Text = "Алгоритм";
            // 
            // grapeAlgRdBtn
            // 
            this.grapeAlgRdBtn.AutoSize = true;
            this.grapeAlgRdBtn.Location = new System.Drawing.Point(151, 29);
            this.grapeAlgRdBtn.Name = "grapeAlgRdBtn";
            this.grapeAlgRdBtn.Size = new System.Drawing.Size(73, 17);
            this.grapeAlgRdBtn.TabIndex = 1;
            this.grapeAlgRdBtn.TabStop = true;
            this.grapeAlgRdBtn.Text = "Виноград";
            this.grapeAlgRdBtn.UseVisualStyleBackColor = true;
            this.grapeAlgRdBtn.Click += new System.EventHandler(this.grapeAlgRdBtn_Click);
            // 
            // ordinaryAlgRdBtn
            // 
            this.ordinaryAlgRdBtn.AutoSize = true;
            this.ordinaryAlgRdBtn.Location = new System.Drawing.Point(7, 29);
            this.ordinaryAlgRdBtn.Name = "ordinaryAlgRdBtn";
            this.ordinaryAlgRdBtn.Size = new System.Drawing.Size(72, 17);
            this.ordinaryAlgRdBtn.TabIndex = 0;
            this.ordinaryAlgRdBtn.TabStop = true;
            this.ordinaryAlgRdBtn.Text = "Обычный";
            this.ordinaryAlgRdBtn.UseVisualStyleBackColor = true;
            this.ordinaryAlgRdBtn.Click += new System.EventHandler(this.ordinaryAlgRdBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 751);
            this.Controls.Add(this.chooseAlgGrpBx);
            this.Controls.Add(this.controlPanel);
            this.Controls.Add(this.chart1);
            this.Name = "MainForm";
            this.Text = "Сравнение эффективности алгоритмов";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.controlPanel.ResumeLayout(false);
            this.controlPanel.PerformLayout();
            this.chooseAlgGrpBx.ResumeLayout(false);
            this.chooseAlgGrpBx.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button analyseButton;
        private System.Windows.Forms.TextBox minTextBox;
        private System.Windows.Forms.TextBox maxTextBox;
        private System.Windows.Forms.TextBox stepTextBox;
        private System.Windows.Forms.Label minLabel;
        private System.Windows.Forms.Label maxLabel;
        private System.Windows.Forms.Label stepLabel;
        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.GroupBox chooseAlgGrpBx;
        private System.Windows.Forms.RadioButton grapeAlgRdBtn;
        private System.Windows.Forms.RadioButton ordinaryAlgRdBtn;
    }
}

