﻿using System.Threading;
using System.Threading.Tasks;

namespace Matrix 
{
    public class MatrixMultiplication
    {
        private MatrixMultiplication() { }
        public static bool CanMultiply<Type>(Type[,] factorA, Type[,] factorB)
        {
            return factorA.GetLength(0) == factorB.GetLength(1) ? true : false;
        }
        public static void Traditional(int[,] result, int[,] factorA, int[,] factorB)
        {
            int m = factorA.GetLength(0);
            int n = factorA.GetLength(1);
            int q = factorB.GetLength(1);
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = 0;
                    for (var k = 0; k < n; ++k)
                        result[i, j] += factorA[i, k] * factorB[k, j];
                }
        }
        private static Task TraditionalPart(int[,] result, int[,] factorA, int[,] factorB, int beg, int count)
        {
            return Task.Run(() =>
                {
                    int n = factorA.GetLength(1);
                    int q = factorB.GetLength(1);
                    int lastIndex = beg + count - 1;
                    for (var i = beg; i <= lastIndex; ++i)
                    {
                        for (var j = 0; j < q; ++j)
                        {
                            result[i, j] = 0;
                            for (var k = 0; k < n; ++k)
                                result[i, j] += factorA[i, k] * factorB[k, j];
                        }
                    }
                }
            );
        }
        public static async Task TraditionalConcurrencyAsync(int[,] result, int[,] factorA, int[,] factorB, int numOfThreads)
        {
            int step = result.GetLength(0) / numOfThreads;
            Task[] tasks = new Task[numOfThreads];
            int lastThIndex = numOfThreads - 1;
            for (var i = 0; i < lastThIndex; ++i)
                tasks[i] = TraditionalPart(result, factorA, factorB, i * step, step);
            tasks[lastThIndex] = TraditionalPart(result, factorA, factorB, lastThIndex * step, result.GetLength(0) - lastThIndex * step);
            await Task.WhenAll(tasks);
        }


        public static void GrapeAlg(int[,] result, int[,] factorA, int[,] factorB, int[] mulH, int[] mulV)
        {
            int m = factorA.GetLength(0), n = factorA.GetLength(1), q = factorB.GetLength(1);
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < n / 2; ++j)
                    mulH[i] += factorA[i, j * 2] * factorA[i, j * 2 + 1];
            for (var i = 0; i < q; ++i)
                for (var j = 0; j < n / 2; ++j)
                    mulV[i] += factorB[j * 2, i] * factorB[j * 2 + 1, i];
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] = -mulH[i] - mulV[j];
                    for (var k = 0; k < n / 2; ++k)
                        result[i, j] += (factorA[i, k * 2] + factorB[k * 2 + 1, j]) * (factorA[i, k * 2 + 1] + factorB[k * 2, j]);
                }
            if(n % 2 == 1)
            {
                for (var i = 0; i < m; ++i)
                    for (var j = 0; j < q; ++j)
                        result[i, j] += factorA[i, n - 1] * factorB[n - 1, j];
            }
        }

        public static void OpGrapeAlg(int[,] result, int[,] factorA, int[,] factorB, 
            int[] mulH, int[] mulV)
        {
            int m = factorA.GetLength(0), n = factorA.GetLength(1), q = factorB.GetLength(1);
            bool odd = n % 2 == 1;
            int idx = n - 1;
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulH[i] -= factorA[i, j] * factorA[i, j + 1];
            for (var i = 0; i < q; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulV[i] -= factorB[j, i] * factorB[j + 1, i];
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < q; ++j)
                {
                    result[i, j] += mulH[i] + mulV[j];
                    for (var k = 0; k < idx; k += 2)
                        result[i, j] += (factorA[i, k] + factorB[k + 1, j]) * (factorA[i, k + 1] + factorB[k, j]);
                    if (odd)
                        result[i, j] += factorA[i, idx] * factorB[idx, j];
                }
 
        }
        private static Task OpGrapeAlgPart(int[,] result, int[,] factorA, int[,] factorB, int[] mulH, int[] mulV,
            int beg, int count, bool odd, int idx)
        {
            return Task.Run(() =>
                {
                    int n = factorA.GetLength(1), q = factorB.GetLength(1);
                    int lastIndex = beg + count - 1;
                    for (var i = beg; i <= lastIndex; ++i)
                        for (var j = 0; j < q; ++j)
                        {
                            result[i, j] += mulH[i] + mulV[j];
                            for (var k = 0; k < idx; k += 2)
                                result[i, j] += (factorA[i, k] + factorB[k + 1, j]) * (factorA[i, k + 1] + factorB[k, j]);
                            if (odd)
                                result[i, j] += factorA[i, idx] * factorB[idx, j];
                        }
                }
            );
        }
        public static async Task OpGrapeAlgConcurrencyAsync(int[,] result, int[,] factorA, int[,] factorB, int numOfThreads,
            int[] mulH, int[] mulV)
        {
            int m = factorA.GetLength(0), n = factorA.GetLength(1), q = factorB.GetLength(1);
            bool odd = n % 2 == 1;
            int idx = n - 1;
            for (var i = 0; i < m; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulH[i] -= factorA[i, j] * factorA[i, j + 1];
            for (var i = 0; i < q; ++i)
                for (var j = 0; j < idx; j += 2)
                    mulV[i] -= factorB[j, i] * factorB[j + 1, i];
            int step = m / numOfThreads;
            Task[] tasks = new Task[numOfThreads];
            int lastThIndex = numOfThreads - 1;
            for (var i = 0; i < lastThIndex; ++i)
                tasks[i] = OpGrapeAlgPart(result, factorA, factorB, mulH, mulV, i * step, step, odd, idx); 
            tasks[lastThIndex] = TraditionalPart(result, factorA, factorB, lastThIndex * step, result.GetLength(0) - lastThIndex * step);
            await Task.WhenAll(tasks);
        }
    }
}
