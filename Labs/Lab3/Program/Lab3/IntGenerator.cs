﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix 
{
    class IntGenerator : IElementGenerator<int>
    {
        private int minValue;
        private int maxValue;
        private Random rand = new Random();
        public IntGenerator(int minValue, int maxValue)
        {
            this.minValue = minValue;
            this.maxValue = maxValue;
        }
        public int generateElement()
        {
            return rand.Next(minValue, maxValue + 1);
        }
    }
}
