﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    public delegate Task<Point[]> TesterMatrDelegate(int size);
    public class Point 
    {
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
        public double X { get; private set; }
        public double Y { get; private set; }
    }
    public class Tester
    {
        private const int numOfSamples = 6;
        private Point[] samples =  new Point[numOfSamples];
        private int[] numOfThr = { 1, 2, 4, 8, 16};
        private int[,] factorA;
        private int[,] factorB;
        private MatrixGenerator<int> matrGenerator; 
        private double ConvertToMilliseconds(TimeSpan duration)
        {
            return duration.Milliseconds + 1000.0 * (duration.Seconds + duration.Minutes * 60.0); 
        }
        public Tester(int minValue, int maxValue)
        {
            matrGenerator = new MatrixGenerator<int>(new IntGenerator(minValue, maxValue));
        }
        public Point testTraditional(int[,] result, int[,] factorA, int[,] factorB)
        {
            DateTime start = DateTime.Now;
            MatrixMultiplication.Traditional(result, factorA, factorB);
            return new Point(result.GetLength(0), ConvertToMilliseconds(DateTime.Now - start));
        }
        public Point testOpGrapeAlg(int[,] result, int[,] factorA, int[,] factorB, int[] mulH, int[] mulV)
        {
            DateTime start = DateTime.Now;
            MatrixMultiplication.OpGrapeAlg(result, factorA, factorB, mulH, mulV);
            return new Point(result.GetLength(0), ConvertToMilliseconds(DateTime.Now - start));
        }
        public async Task<Point> testOpGrapeAlgConcurrency(int numOfThreads, int[,] result, int[,] factorA, int[,] factorB, int[] mulH, int[] mulV)
        {
            DateTime start = DateTime.Now;
            await MatrixMultiplication.OpGrapeAlgConcurrencyAsync(result, factorA, factorB, numOfThreads, mulH, mulV);
            return new Point(result.GetLength(0), ConvertToMilliseconds(DateTime.Now - start));
        }
        public async Task<Point> testTraditionalConcurrency(int numOfThreads, int[,] result, int[,] factorA, int[,] factorB)
        {
            DateTime start = DateTime.Now;
            await MatrixMultiplication.TraditionalConcurrencyAsync(result, factorA, factorB, numOfThreads);
            return new Point(result.GetLength(0), ConvertToMilliseconds(DateTime.Now - start));
        }

        public async Task<Point[]> testAllTraditional(int size)
        {
            factorA = matrGenerator.generateMatrix(size, size);
            factorB = matrGenerator.generateMatrix(size, size);
            int[,] result = new int[factorA.GetLength(0),factorB.GetLength(1)];
            samples[0] = testTraditional(result, factorA, factorB);
            for (var i = 1; i < numOfSamples; ++i)
                samples[i] = await testTraditionalConcurrency(numOfThr[i - 1], result, factorA, factorB);
            return samples; 
        }

        public async Task<Point[]> testAllOpGrapeAlg(int size)
        {
            factorA = matrGenerator.generateMatrix(size, size);
            factorB = matrGenerator.generateMatrix(size,size);
            int[,] result = new int[factorA.GetLength(0),factorB.GetLength(1)];
            int[] mulH = new int[factorA.GetLength(0)];
            int[] mulV = new int[factorB.GetLength(1)];
            samples[0] = testOpGrapeAlg(result, factorA, factorB, mulH, mulV);
            for (var i = 1; i < numOfSamples; ++i)
                samples[i] = await testOpGrapeAlgConcurrency(numOfThr[i-1], result, factorA, factorB, mulH, mulV); 
            return samples; 
        }
    }
}
